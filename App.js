/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import MainScreen from './src/screens/MainScreen';
import EventScreen from './src/screens/EventScreen';
import WebViewPage from './src/screens/WebViewScreen';
import ArchiveScreen from './src/screens/ArchiveScreen';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode="none">
        <Stack.Screen name="Main" component={MainScreen} />
        <Stack.Screen name="Event" component={EventScreen} />
        <Stack.Screen name="WebView" component={WebViewPage} />
        <Stack.Screen name="Archive" component={ArchiveScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
