/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
// import MainScreen from './src/screens/MainScreen';
// import EventScreen from './src/screens/EventScreen';
import 'react-native-gesture-handler';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
