import axios from 'axios';
import baseURL from '../../configs/api';

const http = axios.create({
  withCredentials: true,
  baseURL,
  headers: {'Content-Type': 'application/json', 'cache-control': 'no-cache'},
});

export default http;
