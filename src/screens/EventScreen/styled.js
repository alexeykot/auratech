import styled from 'styled-components';
import {Dimensions, Platform} from 'react-native';
import { isSoftMenuExist } from '../../helpers';

export const Container = styled.View`
  flex: 1;
  width: 100%;
  height: 100%;
`;

export const BackgroundImage = styled.ImageBackground`
  width: 100%;
  height: 50%;
  position: relative;
`;

export const InfoContainer = styled.View`
  position: absolute;
  top: ${({titleHeight}) =>
    Dimensions.get('screen').height / 2 - (75 + titleHeight)}px;
  align-self: center;
  width: 300px;
  /* height: 400px; */
  z-index: 123;
`;

export const Title = styled.Text`
  font-style: normal;
  font-weight: bold;
  font-size: 22px;
  line-height: 24px;
  color: white;
  margin-bottom: 20px;
`;

export const OptionsContainer = styled.ScrollView`
  width: 100%;
  /* max-height: 332px; */
  max-height: ${Dimensions.get('screen').height / 2 -
    (isSoftMenuExist() ? 20 : -25)}px;
  /* VREMENNO */
  background-color: white;
  border-radius: 4px;
`;

export const Row = styled.TouchableOpacity`
  width: 100%;
  flex-direction: row;
  border: 1px solid #e5e5e5;
  padding: 14px 15px 21px 18px;
  border-bottom-width: ${({isLast}) => (isLast ? '1px' : 0)};
  border-top-left-radius: ${({isFirst}) => (isFirst ? '4px' : 0)};
  border-top-right-radius: ${({isFirst}) => (isFirst ? '4px' : 0)};
  border-bottom-left-radius: ${({isLast}) => (isLast ? '4px' : 0)};
  border-bottom-right-radius: ${({isLast}) => (isLast ? '4px' : 0)};
`;

export const DetailRow = styled.View`
  width: 100%;
  border: 1px solid #e5e5e5;
  background-color: white;
  border-bottom-width: 1px;
  padding: 14px 15px 21px 18px;
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
  border-bottom-left-radius: ${({isLast}) => (isLast ? '4px' : 0)};
  border-bottom-right-radius: ${({isLast}) => (isLast ? '4px' : 0)};
`;

export const TimeContainer = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const Date = styled.Text`
  font-style: normal;
  font-weight: bold;
  font-size: 15px;
  line-height: 18px;
  color: #028bd2;
`;

export const Time = styled.Text`
  font-style: normal;
  font-weight: bold;
  font-size: 15px;
  line-height: 18px;
  color: black;
`;

export const PlaceContainer = styled.View`
  display: flex;
  flex-direction: row;
  width: 100%;
  margin-top: 22px;
`;

export const PinIcon = styled.Image`
  width: 15px;
  height: 15px;
`;

export const Place = styled.Text`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 17px;
  opacity: 0.5;
  margin-left: 12px;
  padding-right: 15px;
  /* VREMENNO */
`;

export const Option = styled.Text`
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  line-height: 22px;
  color: black;
  margin-left: 15px;
`;

export const OptionIcon = styled.Image`
  width: 15px;
  height: 15px;
`;

export const RoundButton = styled.TouchableOpacity`
  width: 50px;
  height: 50px;
  border-radius: 50px;
  background-color: black;
  opacity: 0.3;
  position: absolute;
  top: ${Platform.OS === 'android' ? '20px' : '20px'};
  /* left: 25px; */
  left: ${({ isLeft }) => isLeft ? 25 : (Dimensions.get('screen').width - 75)}px;
  justify-content: center;
  align-items: center;
`;

export const GradientImage = styled.Image`
  position: absolute;
  bottom: 0;
  width: 100%;
  left: 0;
  right: 0;
`;

export const Icon = styled.Image`
  /* position: absolute;
  top: 35px;
  left: ${({ isLeft }) => isLeft ? 40 : (Dimensions.get('screen').width - 75)}px; */
  height: 20px;
  width: 20px;
`;

export const IconTouchable = styled.TouchableOpacity`
  position: absolute;
  top: 35px;
  left: ${({ isLeft }) => isLeft ? 40 : (Dimensions.get('screen').width - 60)}px;
`;
