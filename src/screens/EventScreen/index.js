import React from 'react';

import {SvgUri} from 'react-native-svg';

import StatusBar from '../../components/StatusBar';

import http from '../../services/http';

import pin from '../../assets/images/pin.png';
import Pin from '../../assets/icons/pin.svg';
import gradient from '../../assets/images/gradient.png';
import back from '../../assets/images/back.png';
import Back from '../../assets/icons/back.svg';
import internet from '../../assets/images/internet.png';

import * as S from './styled';
import {formatDate} from '../../helpers';

class EventScreen extends React.Component {
  state = {
    titleHeight: 0,
  };
  render() {
    const {
      route: {
        params: {event},
      },
      navigation,
    } = this.props;
    const { titleHeight } = this.state;
    // MOVE TO HELPER
    const options = Object.keys(event.menu).map(k => event.menu[k]);
    const {year, day, month, time} = formatDate(event.beginDate);
    const finishDate = formatDate(event.finishDate);
    return (
      <>
        <StatusBar backgroundColor="#028BD2" barStyle="dark-content" />
        <S.Container>
          <S.BackgroundImage
            source={{
              uri: event.banner,
            }}>
            <S.GradientImage source={gradient} />
          </S.BackgroundImage>
          <S.RoundButton isLeft onPress={() => navigation.goBack()} />
          <S.IconTouchable onPress={() => navigation.goBack()} isLeft>
            <Back width={20} height={20} />
          </S.IconTouchable>
          <S.InfoContainer titleHeight={titleHeight}>
            <S.Title
              onLayout={e =>
                this.setState({titleHeight: e.nativeEvent.layout.height})
              }>
              {event.title}
            </S.Title>
            <S.OptionsContainer stickyHeaderIndices={[0]}>
              <S.DetailRow>
                <S.TimeContainer>
                  <S.Date>
                    {!event.isDateSame && 'С '}
                    {`${day} ${month.toUpperCase()} ${year}`}
                  </S.Date>
                  <S.Time>{time}</S.Time>
                </S.TimeContainer>
                {!event.isDateSame && (
                  <S.Date>
                    {'по '}
                    {`${finishDate.day} ${finishDate.month.toUpperCase()} ${
                      finishDate.year
                    }`}
                  </S.Date>
                )}
                <S.PlaceContainer>
                  <Pin width={15} height={15} />
                  <S.Place>{event.place}</S.Place>
                </S.PlaceContainer>
              </S.DetailRow>
              {options.map((option, key) => (
                <S.Row
                  onPress={() =>
                    navigation.navigate('WebView', {
                      url: option.url,
                      title: event.title,
                    })
                  }
                  key={option.icon}
                  isLast={options.length === key + 1}>
                  <SvgUri
                    width="15px"
                    height="15px"
                    uri={option.icon}
                    style={{marginTop: 4}}
                  />
                  <S.Option>{option.title}</S.Option>
                </S.Row>
              ))}
            </S.OptionsContainer>
          </S.InfoContainer>
        </S.Container>
      </>
    );
  }
}

export default EventScreen;
