/* eslint-disable react/no-did-mount-set-state */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import axios from 'axios';
import {
  SafeAreaView,
  ScrollView,
  ImageBackground,
  Animated,
  Dimensions,
} from 'react-native';
import Moment from 'moment';
import {animatedTimingTransform} from '../../helpers';
import EventCard from '../../components/EventCard';
import StatusBar from '../../components/StatusBar';
import bg from '../../assets/images/bg.png';
// import logo from '../../assets/images/logo.png';
import Logo from '../../assets/icons/logo.svg';
import http from '../../services/http';
import * as S from './styled';

const ALogoContainer = Animated.createAnimatedComponent(S.LogoContainer);
const AWhiteView = Animated.createAnimatedComponent(S.WhiteView);
const ASubTitle = Animated.createAnimatedComponent(S.SubTitle);
const AFakeContainer = Animated.createAnimatedComponent(S.FakeContainer);

const SCROLL_HEIGHT = 200;
// const HEADER_MIN_HEIGHT = 60;
// const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
const finalTopPosition =
  Dimensions.get('window').height / 2 - Dimensions.get('window').height / 3;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.fieldOne = React.createRef();
    this.startOffset = 0;
    this.finalOffset = 0;
    this.isRendered = false;
  }
  state = {
    eventsList: [],
    scrollY: new Animated.Value(0),
    ALogoOpacity: new Animated.Value(0),
    ATitleOpacity: new Animated.Value(0),
    marginBottom: new Animated.Value(150 * Dimensions.get('screen').scale),
    marginTop: new Animated.Value(100 * Dimensions.get('screen').scale),
    ATop: new Animated.Value(Dimensions.get('window').height / 2 - 150),
    marginBottomLogo: new Animated.Value(0),
    // marginTop: new Animated.Value(300),
    isRendered: false,
    archiveEvents: [],
  };
  async componentDidMount() {
    const data = await http.get('/list').catch(err => console.log('err', err));
    if (data !== undefined) {
      const eventsArr = Object.keys(data.data).map(k => data.data[k]);
      this.setState({eventsList: eventsArr});
    }
    animatedTimingTransform(this.state.ALogoOpacity, 1, 2000);
    animatedTimingTransform(this.state.ATitleOpacity, 1, 2000);
    animatedTimingTransform(this.state.marginBottom, 100, 500, 2000);
    animatedTimingTransform(this.state.marginTop, 50, 500, 2000);
    animatedTimingTransform(this.state.ATop, finalTopPosition, 500, 2000);
    setTimeout(() => {
      this.setState({isRendered: true});
    }, 3000);
  }
  offset = 0;

  handleScrollBeginDrag = event => {
    this.startOffset = event.nativeEvent.contentOffset.y;
  };
  handleScrollEndDrag = event => {
    if (event.nativeEvent.contentOffset.y - this.startOffset < 0) {
      if (event.nativeEvent.contentOffset.y < 300) {
        this.fieldOne.scrollTo({x: 0, y: 0, animated: true});
      }
    } else {
      if (event.nativeEvent.contentOffset.y < 300) {
        this.fieldOne.scrollTo({x: 0, y: 300, animated: true});
      }
    }
  };
  sortByDate = eventsList => {
    const sortedArray = eventsList.sort(
      (a, b) =>
        new Moment(a.beginDate).format('YYYYMMDD') -
        new Moment(b.beginDate).format('YYYYMMDD'),
    );
    sortedArray.forEach(event => {
      if (Moment(event.finishDate).isBefore()) {
        // sortedArray.splice(sortedArray.indexOf(event), 1);
        // sortedArray.push(event);
        return (event.isFinished = true);
      } else {
        return (event.isFinished = false);
      }
    });
    sortedArray.forEach(event => {
      if (Moment(event.beginDate).isSame(Moment(event.finishDate), 'day')) {
        return (event.isDateSame = true);
      } else {
        return (event.isDateSame = false);
      }
    });
    return sortedArray;
  };

  render() {
    const {eventsList, isRendered} = this.state;
    const {navigation} = this.props;
    const imageOpacity = this.state.scrollY.interpolate({
      inputRange: [0, SCROLL_HEIGHT],
      outputRange: [1, 0],
      extrapolate: 'clamp',
    });
    const viewOpacity = this.state.scrollY.interpolate({
      inputRange: [0, SCROLL_HEIGHT],
      outputRange: [0, 1],
      extrapolate: 'clamp',
    });
    const finalTop = this.state.scrollY.interpolate({
      inputRange: [0, SCROLL_HEIGHT * 3],
      outputRange: [finalTopPosition, 0],
    });
    const sorted = this.sortByDate(eventsList);
    const archiveEvents = sorted.filter(event => event.isFinished);
    return (
      <>
        <StatusBar backgroundColor="#028BD2" barStyle="dark-content" />
        <SafeAreaView>
          <ImageBackground source={bg} style={{width: '100%', height: '100%'}}>
            <AWhiteView style={{opacity: viewOpacity}} />
            <ALogoContainer
              style={{
                opacity: isRendered ? imageOpacity : this.state.ALogoOpacity,
                top: isRendered ? finalTop : this.state.ATop,
              }}>
              <Logo width={110} height={68} />
              <S.TitleContainer>
                <S.Title>РЕАБИЛИТАЦИОННАЯ ИНДУСТРИЯ РОССИИ</S.Title>
              </S.TitleContainer>
              <ASubTitle
                style={{
                  opacity: isRendered ? imageOpacity : this.state.ATitleOpacity,
                }}>
                КАЛЕНДАРЬ МЕРОПРИЯТИЙ
              </ASubTitle>
            </ALogoContainer>
            <ScrollView
              scrollEventThrottle={16}
              contentInsetAdjustmentBehavior="automatic"
              contentContainerStyle={{
                paddingLeft: 20,
                paddingRight: 20,
                paddingVertical: 40,
              }}
              ref={ref => {
                this.fieldOne = ref;
              }}
              onScrollBeginDrag={this.handleScrollBeginDrag}
              onScrollEndDrag={this.handleScrollEndDrag}
              onScroll={Animated.event([
                {nativeEvent: {contentOffset: {y: this.state.scrollY}}},
              ])}>
              <AFakeContainer
                style={{
                  marginTop: this.state.marginTop,
                  marginBottom: this.state.marginBottom,
                }}
              />
              {!!eventsList.length &&
                sorted
                  .filter(event => !event.isFinished)
                  .map(event => (
                    <EventCard
                      isOne={eventsList.length === 1}
                      key={event.id}
                      onEventPress={() =>
                        navigation.navigate('Event', {event: event})
                      }
                      event={event}
                    />
                  ))}
              <S.ArchiveButton
                onPress={() =>
                  navigation.navigate('Archive', {events: archiveEvents})
                }>
                <S.ArchiveText>Архив</S.ArchiveText>
              </S.ArchiveButton>
            </ScrollView>
          </ImageBackground>
        </SafeAreaView>
      </>
    );
  }
}

export default App;
