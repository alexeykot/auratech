import styled from 'styled-components';
import { Dimensions } from 'react-native';

export const Container = styled.View`
  flex: 1;
`;

export const Background = styled.ImageBackground`
  position: absolute;
  top: 0;
  width: 100%;
  height: 100%;
`;

export const WhiteView = styled.View`
  position: absolute;
  top: 0;
  width: 100%;
  height: 100%;
  background-color: white;
`;

export const FakeContainer = styled.View`
  height: 150px;
  width: 100%;
`;

export const LogoContainer = styled.View`
  padding: 0px 50px 0 50px;
  align-self: center;
  align-items: center;
  width: 100%;
  position: absolute;
`;

export const Logo = styled.Image`
  width: 110px;
  height: 68px;
  align-self: center;
`;

export const Title = styled.Text`
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 18px;
  color: #028BD2;
  text-align: center;
`;

export const TitleContainer = styled.View`
  margin-top: 15px;
  width: 100%;
  height: 40px;

`;

export const SubTitle = styled.Text`
  font-style: normal;
  font-weight: 900;
  font-size: 10px;
  line-height: 18px;
  color: #E83548;
  text-align: center;
  z-index: 123123;
`;

export const ArchiveButton = styled.TouchableOpacity`
  width: 50%;
  height: 50px;
  border: 1px solid #028BD2;
  border-radius: 5px;
  align-self: center;
  justify-content: center;
  align-items: center;
  text-align: center;
  padding-bottom: 5px;
  margin-bottom: 150px;
`;

export const ArchiveText = styled.Text`
  color: #028BD2;
  font-size: 20px;
  text-align: center;
`;
