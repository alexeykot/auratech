/* eslint-disable react/no-did-mount-set-state */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {SafeAreaView, ScrollView} from 'react-native';
import Moment from 'moment';
import EventCard from '../../components/EventCard';
import StatusBar from '../../components/StatusBar';
import Header from './Header';
import Back from '../../assets/icons/backBlack1.svg';
import * as S from './styled';

class ArchiveScreen extends React.Component {
  state = {
    eventsList: [],
  };
  async componentDidMount() {
    // if (data !== undefined) {
    //   const eventsArr = Object.keys(data.data).map(k => data.data[k]);
    //   this.setState({eventsList: eventsArr});
    // }
  }

  sortByDate = eventsList => {
    const sortedArray = eventsList.sort(
      (a, b) =>
        new Moment(a.beginDate).format('YYYYMMDD') -
        new Moment(b.beginDate).format('YYYYMMDD'),
    );
    return sortedArray;
  };

  render() {
    const {eventsList} = this.state;
    const {
      navigation,
      route: {
        params: {events},
      },
    } = this.props;
    const sorted = this.sortByDate(events);
    return (
      <>
        <StatusBar backgroundColor="#028BD2" barStyle="dark-content" />
        <Header
          title="Прошедшие мероприятия"
          onBackPress={() => navigation.goBack()}
        />
        <SafeAreaView>
          <ScrollView
            scrollEventThrottle={16}
            contentInsetAdjustmentBehavior="automatic"
            contentContainerStyle={{
              paddingLeft: 20,
              paddingRight: 20,
              paddingVertical: 20,
              paddingBottom: 80,
            }}>
            {!!sorted.length &&
              sorted.map(event => (
                <EventCard
                  isOne={eventsList.length === 1}
                  key={event.id}
                  onEventPress={() =>
                    navigation.navigate('Event', {event: event})
                  }
                  event={event}
                />
              ))}
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
}

export default ArchiveScreen;
