import styled from 'styled-components';

export const BackButton = styled.TouchableOpacity`
  padding-top: 2px;
  position: absolute;
  top: 40px;
  left: 20px;
  height: 50px;
  width: 50px;
  background-color: transparent;
  justify-content: center;
  align-items: center;
  z-index: 12312313;
`;
