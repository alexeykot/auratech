import styled from 'styled-components';

import {Dimensions} from 'react-native';

// const width = Dimensions.get('screen').width;
export const Container = styled.View`
  width: 100%;
  height: 60px;
  padding-top: 5px;
  background-color: white;
  justify-content: center;
  margin-top: ${({isMargined}) => (isMargined ? '20px' : 0)};
  flex-direction: row;
  align-items: center;
  display: flex;
  border: 1px solid #e5e5e5;
  border-left-width: 0;
  border-right-width: 0;
  border-top-width: 0;
`;

export const Title = styled.Text`
  display: flex;
  overflow: hidden;
  /* margin-bottom: auto;
  margin-top: auto; */
  align-self: center;
  max-width: 225px;
  font-size: 18px;
  margin-bottom: 3px;
`;

export const BackButton = styled.TouchableOpacity`
  position: absolute;
  left: 0;
  padding-top: 2px;
  height: 60px;
  width: 50px;
  background-color: transparent;
  justify-content: center;
  align-items: center;
`;
