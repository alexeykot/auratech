import React from 'react';

import Back from '../../../assets/icons/backBlack1.svg';
import List from '../../../assets/icons/list1.svg';
import * as S from './styled';

const Header = props => {
  const {onBackPress, onHomePress, title, isHomeVisible, isMargined} = props;
  return (
    <S.Container isMargined={isMargined}>
      <S.BackButton onPress={() => onBackPress()}>
        <Back width={20} height={20} />
      </S.BackButton>
      <S.Title numberOfLines={1}>{title}</S.Title>
      {isHomeVisible && (
        <S.HomeButton onPress={() => onHomePress()}>
          <List width={20} height={20} />
        </S.HomeButton>
      )}
    </S.Container>
  );
};

export default Header;
