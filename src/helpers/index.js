import {Animated, Platform, Dimensions} from 'react-native';

export const animatedTimingTransform = (option, toValue, duration, delay = 0) =>
  Animated.timing(option, {
    toValue,
    duration,
    delay,
  }).start();

const monthes = {
  '01': 'Января',
  '02': 'Февраля',
  '03': 'Марта',
  '04': 'Апреля',
  '05': 'Мая',
  '06': 'Июня',
  '07': 'Июля',
  '08': 'Августа',
  '09': 'Сентября',
  '10': 'Октября',
  '11': 'Ноября',
  '12': 'Декабря',
};

export const formatDate = inputDate => {
  const formmattedString = inputDate.split('-');
  const day = formmattedString[2].split(' ')[0];
  const time = formmattedString[2].split(' ')[1].slice(0, 5);
  const year = formmattedString[0];
  const month = monthes[formmattedString[1]];
  return {
    day,
    month,
    year,
    time,
  };
  // return `${day} ${month.toUpperCase()} ${year}`;
};

// export const styleAnimation = (afterAnimation = false) => {
//   if (Platform.OS === 'ios') {
//     if (Dimensions.get('screen').height > 700) {
//       if (afterAnimation) {
//         return
//       }
//     }
//   }
// }

export const isSoftMenuExist = () =>
  Platform.OS === 'android' &&
  Dimensions.get('screen').height - Dimensions.get('window').height > 0;

export const fu = () => {};
