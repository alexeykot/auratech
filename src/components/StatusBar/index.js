import React from 'react';

import {
  View,
  StatusBar,
  StyleSheet,
  Platform,
  Dimensions,
  PixelRatio,
} from 'react-native';

const MyStatusBar = ({backgroundColor, ...props}) => {
  return (
    <View style={[styles.statusBar, {backgroundColor}]}>
      <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
  );
};

const STATUSBAR_HEIGHT =
  Platform.OS === 'ios' ? 12 * PixelRatio.get() : StatusBar.currentHeight;

const styles = StyleSheet.create({
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
});

export default MyStatusBar;
