import React from 'react';

import {formatDate} from '../../helpers';
import Pin from '../../assets/icons/pin.svg';

import pin from '../../assets/images/pin.png';
import * as S from './styled';

const EventCard = ({event, onEventPress, isOne}) => {
  console.log('evemt', event);
  const {year, day, month} = formatDate(event.beginDate);
  const finishDate = formatDate(event.finishDate);
  return (
    <S.Container
      // isFinished={event.isFinished}
      onPress={onEventPress}
      isOne={isOne}>
      <S.BackgroundView
        source={{
          uri: event.banner,
          // headers: {
          //   Authorization: 'Basic cmlyOjU1',
          // },
        }}
      />
      <S.InfoContainer>
        <S.DateContainer>
          <S.Date>{`${day} ${month.toUpperCase()} ${year}`}</S.Date>
          {!event.isDateSame && (
            <S.Date>
              {' '}
              -{' '}
              {`${finishDate.day} ${finishDate.month.toUpperCase()} ${
                finishDate.year
              }`}
            </S.Date>
          )}
        </S.DateContainer>
        <S.Title>{event.title}</S.Title>
        <S.PlaceContainer>
          <Pin width={15} height={15} />
          <S.Place>{event.place}</S.Place>
        </S.PlaceContainer>
      </S.InfoContainer>
    </S.Container>
  );
};

export default EventCard;
