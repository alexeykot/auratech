import styled from 'styled-components';
import {Dimensions} from 'react-native';

const margin = Dimensions.get('window').height - 325 - 15 - 40;
export const Container = styled.TouchableOpacity`
  width: 100%;
  /* height: 325px; */
  /* min-height: 325px; */
  max-height: 400px;
  border-radius: 4px;
  margin-bottom: ${({isOne}) => (isOne ? margin : 15)};
  border: 0.5px solid #E5E5E5;
  /* opacity: ${({isFinished}) => (isFinished ? 0.3 : 1)}; */
`;

export const BackgroundView = styled.Image`
  /* height: 50%; */
  height: 162px;
  width: 100%;
  border-radius: 4px;
  border-bottom-left-radius: 0px;
  border-bottom-right-radius: 0px;
`;

export const InfoContainer = styled.View`
  width: 100%;
  /* height: 50%; */
  /* height: 150px; */
  background-color: white;
  padding: 10px;
  border-radius: 4px;
  border-top-left-radius: 0px;
  border-top-right-radius: 0px;
  /* border: 0.5px solid #E5E5E5;
  border-top-color: transparent; */
`;

export const Date = styled.Text`
  font-style: normal;
  font-weight: normal;
  font-size: 15px;
  line-height: 18px;
  color: #028BD2;
`;

export const DateContainer = styled.Text`
  flex-direction: row;
`;

export const Title = styled.Text`
  font-style: normal;
  font-weight: bold;
  font-size: 22px;
  line-height: 24px;
  /* ВРЕМЕННО */
  color: black;
  margin-top: 15px;
  max-height: 100px;
  margin-bottom: 15px;
`;

export const PlaceContainer = styled.View`
  display: flex;
  flex-direction: row;
  width: 100%;
  padding-right: 15px;
  overflow: hidden;
  margin-top: auto;
  margin-bottom: auto;
`;

export const PinIcon = styled.Image`
  width: 15px;
  height: 15px;
`;

export const Place = styled.Text`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 17px;
  opacity: 0.5;
  margin-left: 12px;
  /* VREMENNO */
`;
